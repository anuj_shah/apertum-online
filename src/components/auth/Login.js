import React, { Component } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { loginUser } from "../../actions/authActions";
import classnames from "classnames";

class Login extends Component {
  state = {
    accountId: "",
    pswd: "",
    errors: {}
  };

  componentDidMount() {
    // If logged in and user navigates to Login page, should redirect them to restaurants
    if (this.props.auth.isAuthenticated) {
      this.props.history.push("/users");
    }
  }

  static getDerivedStateFromProps(nextProps) {
    if (nextProps.auth.isAuthenticated) {
      nextProps.history.push("/users");
    }

    if (nextProps.errors) {
      return {
        errors: nextProps.errors
      };
    }
  }

  onChange = e => {
    this.setState({ [e.target.id]: e.target.value });
  };

  onSubmit = e => {
    e.preventDefault();

    const { accountId, pswd } = this.state;
    const userData = {
      accountId,
      pswd
    };
    this.props.loginUser(userData);
  };

  render() {
    const { errors, pswd, accountId } = this.state;

    const { auth } = this.props;
    return (
      <div className="container">
        <div style={{ marginTop: "4rem" }} className="row">
          <div className="col s8 offset-s2">
            <Link to="/" className="btn-flat waves-effect">
              <i className="material-icons left">keyboard_backspace</i> Back to
              home
            </Link>
            <form noValidate onSubmit={this.onSubmit}>
              <div className="input-field col s12">
                <input
                  onChange={this.onChange}
                  value={accountId}
                  error={errors.accountId}
                  id="accountId"
                  type="text"
                  className={classnames("", {
                    invalid: errors.accountId
                  })}
                />
                <label htmlFor="accountId">Account Id</label>
                <span className="red-text">{errors.accountId}</span>
              </div>
              <div className="input-field col s12">
                <input
                  onChange={this.onChange}
                  value={pswd}
                  error={errors.pswd}
                  id="pswd"
                  type="password"
                  className={classnames("", {
                    invalid: errors.pswd
                  })}
                />
                <label htmlFor="pswd">Password</label>
                <span className="red-text">{errors.pswd}</span>
              </div>
              <div className="col s12" style={{ paddingLeft: "11.250px" }}>
                <button
                  style={{
                    width: "150px",
                    borderRadius: "3px",
                    letterSpacing: "1.5px",
                    marginTop: "1rem"
                  }}
                  type="submit"
                  className={`btn btn-large waves-effect waves-light hoverable blue accent-3 ${
                    auth.loading ? "disabled" : ""
                  }`}
                >
                  Login
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

Login.propTypes = {
  loginUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(mapStateToProps, { loginUser })(Login);
