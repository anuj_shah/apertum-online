import React, { Component } from "react";
import axios from "axios";

import { Loader } from "../Loader";
import { UserCard } from "./UserCard";
import { cloneDeep } from "lodash";
import { FilterView } from "./FilterView";

class Users extends Component {
  state = { users: [], loading: false };

  async componentDidMount() {
    try {
      this.setState({ loading: true });
      const response = await axios(
        "https://apertum-interview.herokuapp.com/api/users"
      );
      this.setState({ users: response.data, loading: false });
    } catch (e) {
      console.error(e);
      this.setState({ loading: false });
    }
  }

  handleFilter = () => {
    let users = cloneDeep(this.state.users);
    users = users.filter(user => {
      const { lastName, firstName, age } = user;
      const combinedName = `${firstName}${lastName}`;
      return combinedName.length >= 10 && age >= 20 && age < 30;
    });
    this.setState(prevState => ({
      prevUsers: prevState.users,
      users,
      filterApplied: true
    }));
  };

  handleReset = () => {
    this.setState({ users: this.state.prevUsers, filterApplied: false });
  };

  render() {
    const { loading, users, filterApplied } = this.state;

    if (users.length === 0 && !loading)
      return <div className="center">No Users Found.</div>;

    return (
      <div className="container" style={{ width: "100%" }}>
        <div className="row">
          <div className="landing-copy col s12 center-align">
            {loading ? (
              <Loader />
            ) : (
              <React.Fragment>
                <FilterView
                  filterApplied={filterApplied}
                  onFilter={this.handleFilter}
                  onReset={this.handleReset}
                />
                <div className="row">
                  {users.map(user => (
                    <UserCard key={user.accountId} user={user} />
                  ))}
                </div>
              </React.Fragment>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default Users;
