import * as PropTypes from "prop-types";
import React from "react";

export function FilterView(props) {
  return (
    <>
      <a
        style={{ margin: 5 }}
        className={`waves-effect waves-light btn ${
          props.filterApplied ? "disabled" : ""
        }`}
        onClick={props.onFilter}
      >
        Apply Filters
      </a>
      {props.filterApplied && (
        <a
          style={{ margin: 5 }}
          className="waves-effect waves-light btn"
          onClick={props.onReset}
        >
          Clear Filters
        </a>
      )}
    </>
  );
}

FilterView.propTypes = {
  filterApplied: PropTypes.any,
  onFilter: PropTypes.func,
  onReset: PropTypes.func
};
