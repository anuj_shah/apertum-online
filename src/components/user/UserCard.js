import * as PropTypes from "prop-types";
import React from "react";

export function UserCard(props) {
  const { description, firstName, lastName, age } = props.user;
  return (
    <div className="col s12 m6 l4">
      <div className="card blue-grey darken-1">
        <div className="card-content white-text">
          <span className="card-title">
            {firstName} {lastName}
          </span>
          <p>{description}</p>
        </div>
        <div className="card-action">
          <div>Age: {age}</div>
        </div>
      </div>
    </div>
  );
}

UserCard.propTypes = { user: PropTypes.any };
