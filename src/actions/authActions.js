import axios from "axios";
import setAuthToken from "../utils/setAuthToken";
import jwt_decode from "jwt-decode";

import { GET_ERRORS, SET_CURRENT_USER, USER_LOADING } from "./types";

// Login - get user token
export const loginUser = userData => dispatch => {
  dispatch(setUserLoading(true));
  axios
    .post("https://apertum-interview.herokuapp.com/api/user/login", userData)
    .then(res => {
      // Save to localStorage
      // Set token to localStorage
      const { token, error_message } = res.data;
      if (error_message) {
        dispatch({
          type: GET_ERRORS,
          payload: { accountId: error_message }
        });
        dispatch(setUserLoading(false));
        return;
      }
      localStorage.setItem("jwtToken", token);
      // Set token to Auth header
      setAuthToken(token);
      // Decode token to get user data
      const decoded = jwt_decode(token);
      // Set current user
      dispatch(setCurrentUser(decoded));
    })
    .catch(err => {
      dispatch({
        type: GET_ERRORS,
        payload: { pswd: "Incorrect Password" }
      });
      dispatch(setUserLoading(false));
    });
};

// Set logged in user
export const setCurrentUser = decoded => {
  return {
    type: SET_CURRENT_USER,
    payload: decoded
  };
};

// User loading
export const setUserLoading = (value = false) => {
  return {
    type: USER_LOADING,
    payload: value
  };
};

// Log user out
export const logoutUser = () => dispatch => {
  // Remove token from local storage
  localStorage.removeItem("jwtToken");
  // Remove auth header for future requests
  setAuthToken(false);
  // Set current user to empty object {} which will set isAuthenticated to false
  dispatch(setCurrentUser({}));
};
